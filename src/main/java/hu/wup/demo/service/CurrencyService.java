package hu.wup.demo.service;

import hu.wup.demo.domain.Currency;
import hu.wup.demo.domain.CurrencyRate;
import hu.wup.demo.domain.Direction;
import hu.wup.demo.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;
import java.util.function.Function;

import static hu.wup.demo.domain.Currency.HUF;
import static hu.wup.demo.domain.Direction.BUY;
import static hu.wup.demo.domain.Direction.SELL;
import static java.math.BigDecimal.ZERO;
import static java.math.RoundingMode.HALF_UP;
import static java.util.stream.Collectors.toMap;

@Service
public class CurrencyService {

    private final CurrencyRepository currencyRepository;

    @Autowired
    public CurrencyService(CurrencyRepository currencyRepository) {
        this.currencyRepository = currencyRepository;
    }

    public Map<Currency, BigDecimal> listCurrencies(Currency sourceCurrency, Direction direction) {
        return currencyRepository.findBySourceCurrency(sourceCurrency).stream()
                .collect(toMap(CurrencyRate::getTargetCurrency, cr -> determineExchangeRate(direction).apply(cr)));
    }

    public BigDecimal convertCurrency(Currency sourceCurrency, Currency targetCurrency, Direction direction, BigDecimal amount) {
        if (equalsBigDecimal(amount, ZERO)) {
            return ZERO;
        } else if (sourceCurrency.equals(targetCurrency)) {
            return amount;
        } else if (sourceCurrency.equals(HUF) || targetCurrency.equals(HUF)) {
            CurrencyRate currencyRate = currencyRepository.findBySourceCurrencyAndTargetCurrency(sourceCurrency, targetCurrency);
            BigDecimal exchangeRate = determineExchangeRate(currencyRate, direction);
            return amount.multiply(exchangeRate).setScale(4, HALF_UP);
        } else {
            CurrencyRate sourceToHufCurrencyRate = currencyRepository.findBySourceCurrencyAndTargetCurrency(sourceCurrency, HUF);
            CurrencyRate hufToTargetCurrencyRate = currencyRepository.findBySourceCurrencyAndTargetCurrency(HUF, targetCurrency);

            BigDecimal sourceToHufExchangeRate = determineExchangeRate(sourceToHufCurrencyRate, direction);
            BigDecimal hufToTargetExchangeRate = determineExchangeRate(hufToTargetCurrencyRate, direction);
            return amount.multiply(sourceToHufExchangeRate).multiply(hufToTargetExchangeRate).setScale(4, HALF_UP);
        }
    }

    private Function<CurrencyRate, BigDecimal> determineExchangeRate(Direction direction) {
        return direction.equals(BUY) ? CurrencyRate::getBuyExchangeRate : CurrencyRate::getSellExchangeRate;
    }

    private BigDecimal determineExchangeRate(CurrencyRate currencyRate, Direction direction) {
        return direction.equals(SELL) ? currencyRate.getSellExchangeRate() : currencyRate.getBuyExchangeRate();
    }

    private boolean equalsBigDecimal(BigDecimal firstValue, BigDecimal secondValue) {
        return firstValue.compareTo(secondValue) == 0;
    }
}
