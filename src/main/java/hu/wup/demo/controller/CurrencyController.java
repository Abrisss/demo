package hu.wup.demo.controller;

import hu.wup.demo.domain.Currency;
import hu.wup.demo.domain.Direction;
import hu.wup.demo.service.CurrencyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Map;

@RestController
@RequestMapping(path = "/currencies")
@Validated
public class CurrencyController {

    private final CurrencyService currencyService;

    @Autowired
    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @GetMapping
    public Map<Currency, BigDecimal> listCurrencies(@RequestParam("from") @NotNull Currency sourceCurrency,
                                                    @RequestParam("direction") @NotNull Direction direction) {
        return currencyService.listCurrencies(sourceCurrency, direction);
    }

    @GetMapping(path = "/convert")
    public BigDecimal convertCurrency(@RequestParam("from") @NotNull Currency sourceCurrency,
                                      @RequestParam("to") @NotNull Currency targetCurrency,
                                      @RequestParam("direction") @NotNull Direction direction,
                                      @RequestParam("amount") @NotNull @Digits(integer = 15, fraction = 4) @DecimalMin("0") BigDecimal amount) {
        return currencyService.convertCurrency(sourceCurrency, targetCurrency, direction, amount);
    }
}
