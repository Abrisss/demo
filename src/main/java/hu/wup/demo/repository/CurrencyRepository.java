package hu.wup.demo.repository;

import hu.wup.demo.domain.Currency;
import hu.wup.demo.domain.CurrencyRate;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface CurrencyRepository extends JpaRepository<CurrencyRate, Long> {

    Set<CurrencyRate> findBySourceCurrency(Currency sourceCurrency);

    CurrencyRate findBySourceCurrencyAndTargetCurrency(Currency sourceCurrency, Currency targetCurrency);
}
