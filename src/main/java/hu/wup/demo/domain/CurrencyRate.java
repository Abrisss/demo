package hu.wup.demo.domain;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.GenerationType.SEQUENCE;
import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PROTECTED;

@Builder
@AllArgsConstructor(access = PRIVATE)
@RequiredArgsConstructor
@NoArgsConstructor(force = true, access = PROTECTED)
@Getter
@Setter
@ToString
@EqualsAndHashCode(of = {"sourceCurrency", "targetCurrency"})

@Entity
@Table(name = "CURRENCY_RATE")
public class CurrencyRate {
    @Id
    @GeneratedValue(strategy = SEQUENCE)
    @Column(name = "ID")
    private Long id;

    @Enumerated(STRING)
    @Column(name = "SOURCE_CURRENCY", nullable = false, updatable = false)
    private final Currency sourceCurrency;

    @Enumerated(STRING)
    @Column(name = "TARGET_CURRENCY", nullable = false, updatable = false)
    private final Currency targetCurrency;

    @Digits(integer = 15, fraction = 5)
    @Column(name = "SELL_EXCHANGE_RATE", nullable = false, updatable = false)
    private final BigDecimal sellExchangeRate;

    @Digits(integer = 15, fraction = 5)
    @Column(name = "BUY_EXCHANGE_RATE", nullable = false, updatable = false)
    private final BigDecimal buyExchangeRate;
}
