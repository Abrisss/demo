package hu.wup.demo.domain;

public enum Currency {
    HUF,
    USD,
    EUR,
    JPY,
    CAD
}
