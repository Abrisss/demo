package hu.wup.demo.repository;

import hu.wup.demo.BaseDataGenerator;
import hu.wup.demo.domain.CurrencyRate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Set;

import static hu.wup.demo.domain.Currency.*;
import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
@Import(BaseDataGenerator.class)
public class CurrencyRepositoryTest {

    @Autowired
    private BaseDataGenerator baseDataGenerator;

    @Autowired
    private CurrencyRepository underTest;

    @Before
    public void setUp() throws Exception {
        baseDataGenerator.generate();
    }

    @Test
    public void findBySourceCurrency() {
        //GIVEN in setUp()

        //WHEN
        Set<CurrencyRate> result = underTest.findBySourceCurrency(HUF);

        //THEN
        assertThat(result)
                .hasSize(4)
                .extracting(CurrencyRate::getTargetCurrency)
                .containsExactlyInAnyOrder(EUR, USD, CAD, JPY);

    }

    @Test
    public void findBySourceCurrencyAndTargetCurrency() {
        //GIVEN in setUp()

        //WHEN
        CurrencyRate result = underTest.findBySourceCurrencyAndTargetCurrency(EUR, HUF);

        //THEN
        assertThat(result).isNotNull();
        assertThat(result.getSourceCurrency()).isEqualTo(EUR);
        assertThat(result.getTargetCurrency()).isEqualTo(HUF);
        assertThat(result.getSellExchangeRate()).isEqualByComparingTo(new BigDecimal("310"));
        assertThat(result.getBuyExchangeRate()).isEqualByComparingTo(new BigDecimal("320"));
    }
}
