package hu.wup.demo.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import hu.wup.demo.domain.Currency;
import hu.wup.demo.service.CurrencyService;
import io.restassured.module.mockmvc.specification.MockMvcRequestSpecification;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.ConstraintViolationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import static hu.wup.demo.domain.Currency.*;
import static hu.wup.demo.domain.Direction.BUY;
import static hu.wup.demo.domain.Direction.SELL;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static java.math.BigDecimal.ONE;
import static java.math.BigDecimal.TEN;
import static org.apache.http.HttpStatus.SC_BAD_REQUEST;
import static org.apache.http.HttpStatus.SC_OK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@WebMvcTest(CurrencyController.class)
public class CurrencyControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private CurrencyService currencyService;

    @Rule
    public ExpectedException exception = ExpectedException.none();

    private MockMvcRequestSpecification given;

    @Before
    public void setUp() throws Exception {
        given = given()
                .log().all()
                .mockMvc(mockMvc);
    }

    @Test
    public void listCurrenciesShouldThrowExceptionWhenCalledWithNullValue() {
        Throwable thrown = catchThrowable(
                () -> given
                        .param("from", CAD)
                        .param("direction", "")
                        .when()
                        .get("/currencies")
                        .then()
                        .log().all()
                        .and()
                        .statusCode(SC_OK));

        assertThat(thrown).hasCauseInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void listCurrenciesShouldReturnWith400StatusWhenCalledWithNotValidValues() {
        given
                .param("from", "NOT_VALID_FROM")
                .param("direction", "NOT_VALID_DIRECTION")
                .when()
                .get("/currencies")
                .then()
                .log().all()
                .and()
                .statusCode(SC_BAD_REQUEST);
    }

    @Test
    public void listCurrenciesShouldReturnNormally() throws IOException {
        ImmutableMap<Currency, BigDecimal> expectedCurrencyMap = ImmutableMap.of(
                JPY, TEN,
                HUF, TEN,
                EUR, TEN,
                CAD, TEN);
        when(currencyService.listCurrencies(CAD, BUY)).thenReturn(expectedCurrencyMap);

        String responseBodyJson = given
                .param("from", CAD)
                .param("direction", BUY)
                .when()
                .get("/currencies")
                .then()
                .log().all()
                .and()
                .statusCode(SC_OK)
                .and()
                .extract().body().asString();

        Map<Currency, BigDecimal> resultCurrencyMap = new ObjectMapper().readValue(responseBodyJson, new TypeReference<Map<Currency, BigDecimal>>() {
        });
        assertThat(resultCurrencyMap)
                .hasSize(expectedCurrencyMap.size())
                .containsAllEntriesOf(expectedCurrencyMap);
    }


    @Test
    public void convertCurrencyShouldThrowExceptionWhenCalledWithNullValues() {
        Throwable thrown = catchThrowable(
                () -> given
                        .param("from", "")
                        .param("to", HUF)
                        .param("direction", "")
                        .param("amount", ONE)
                        .when()
                        .get("/currencies/convert")
                        .then()
                        .log().all()
                        .and()
                        .statusCode(SC_OK));

        assertThat(thrown).hasCauseInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void convertCurrencyShouldThrowExceptionWhenCalledWithAmountWithInvalidFraction() {
        Throwable thrown = catchThrowable(
                () -> given
                        .param("from", CAD)
                        .param("to", HUF)
                        .param("direction", SELL)
                        .param("amount", new BigDecimal("11.111111"))
                        .when()
                        .get("/currencies/convert")
                        .then()
                        .log().all()
                        .and()
                        .statusCode(SC_OK));

        assertThat(thrown).hasCauseInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void convertCurrencyShouldThrowExceptionWhenCalledWithAmountWithInvalidInteger() {
        Throwable thrown = catchThrowable(
                () -> given
                        .param("from", CAD)
                        .param("to", HUF)
                        .param("direction", SELL)
                        .param("amount", new BigDecimal("1111111111111111.11"))
                        .when()
                        .get("/currencies/convert")
                        .then()
                        .log().all()
                        .and()
                        .statusCode(SC_OK));

        assertThat(thrown).hasCauseInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void convertCurrencyShouldThrowExceptionWhenCalledWithNegativeAmount() {
        Throwable thrown = catchThrowable(
                () -> given
                        .param("from", CAD)
                        .param("to", HUF)
                        .param("direction", SELL)
                        .param("amount", new BigDecimal("-1"))
                        .when()
                        .get("/currencies/convert")
                        .then()
                        .log().all()
                        .and()
                        .statusCode(SC_OK));

        assertThat(thrown).hasCauseInstanceOf(ConstraintViolationException.class);
    }

    @Test
    public void convertCurrencyShouldReturnWith400WhenCalledWithNotValidValue() {
        given
                .param("from", "NOT_VALID_FROM")
                .param("to", HUF)
                .param("direction", SELL)
                .param("amount", ONE)
                .when()
                .get("/currencies/convert")
                .then()
                .log().all()
                .and()
                .statusCode(SC_BAD_REQUEST);
    }


    @Test
    public void convertCurrencyShouldReturnNormally() {
        BigDecimal amountWithFourScale = new BigDecimal("111111.1111");
        when(currencyService.convertCurrency(CAD, HUF, SELL, amountWithFourScale)).thenReturn(TEN);

        BigDecimal result = given
                .param("from", CAD)
                .param("to", HUF)
                .param("direction", SELL)
                .param("amount", amountWithFourScale)
                .when()
                .get("/currencies/convert")
                .then()
                .log().all()
                .and()
                .statusCode(SC_OK)
                .and()
                .extract().body().as(BigDecimal.class);

        assertThat(result).isEqualByComparingTo(TEN);
    }
}
