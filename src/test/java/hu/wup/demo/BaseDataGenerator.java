package hu.wup.demo;

import hu.wup.demo.domain.CurrencyRate;
import hu.wup.demo.repository.CurrencyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.math.MathContext;

import static hu.wup.demo.domain.Currency.*;
import static java.math.BigDecimal.ONE;
import static java.math.RoundingMode.HALF_UP;

@Component
public class BaseDataGenerator {

    private static final MathContext MATH_CONTEXT = new MathContext(20, HALF_UP);

    @Autowired
    private CurrencyRepository currencyRepository;

    public void generate() {
        BigDecimal usdToHufSellExchangeRate = new BigDecimal("250");
        BigDecimal usdToHufBuyExchangeRate = new BigDecimal("260");
        currencyRepository.save(CurrencyRate.builder()
                .sourceCurrency(USD)
                .targetCurrency(HUF)
                .sellExchangeRate(usdToHufSellExchangeRate)
                .buyExchangeRate(usdToHufBuyExchangeRate)
                .build());

        currencyRepository.save(CurrencyRate.builder()
                .sourceCurrency(HUF)
                .targetCurrency(USD)
                .sellExchangeRate(oneDividedBy(usdToHufSellExchangeRate))
                .buyExchangeRate(oneDividedBy(usdToHufBuyExchangeRate))
                .build());

        BigDecimal eurToHufSellExchangeRate = new BigDecimal("310");
        BigDecimal eurToHufBuyExchangeRate = new BigDecimal("320");
        currencyRepository.save(CurrencyRate.builder()
                .sourceCurrency(EUR)
                .targetCurrency(HUF)
                .sellExchangeRate(eurToHufSellExchangeRate)
                .buyExchangeRate(eurToHufBuyExchangeRate)
                .build());

        currencyRepository.save(CurrencyRate.builder()
                .sourceCurrency(HUF)
                .targetCurrency(EUR)
                .sellExchangeRate(oneDividedBy(eurToHufSellExchangeRate))
                .buyExchangeRate(oneDividedBy(eurToHufBuyExchangeRate))
                .build());

        BigDecimal cadToHufSellExchangeRate = new BigDecimal("200");
        BigDecimal cadToHufBuyExchangeRate = new BigDecimal("210");
        currencyRepository.save(CurrencyRate.builder()
                .sourceCurrency(CAD)
                .targetCurrency(HUF)
                .sellExchangeRate(cadToHufSellExchangeRate)
                .buyExchangeRate(cadToHufBuyExchangeRate)
                .build());

        currencyRepository.save(CurrencyRate.builder()
                .sourceCurrency(HUF)
                .targetCurrency(CAD)
                .sellExchangeRate(oneDividedBy(cadToHufSellExchangeRate))
                .buyExchangeRate(oneDividedBy(cadToHufBuyExchangeRate))
                .build());

        BigDecimal jpyToHufSellExchangeRate = new BigDecimal("130");
        BigDecimal jpyToHufBuyExchangeRate = new BigDecimal("140");
        currencyRepository.save(CurrencyRate.builder()
                .sourceCurrency(JPY)
                .targetCurrency(HUF)
                .sellExchangeRate(jpyToHufSellExchangeRate)
                .buyExchangeRate(jpyToHufBuyExchangeRate)
                .build());

        currencyRepository.save(CurrencyRate.builder()
                .sourceCurrency(HUF)
                .targetCurrency(JPY)
                .sellExchangeRate(oneDividedBy(jpyToHufSellExchangeRate))
                .buyExchangeRate(oneDividedBy(jpyToHufBuyExchangeRate))
                .build());

    }

    private BigDecimal oneDividedBy(BigDecimal jpyToHufSellExchangeRate) {
        return ONE.divide(jpyToHufSellExchangeRate, MATH_CONTEXT).setScale(5, HALF_UP);
    }
}
