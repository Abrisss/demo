package hu.wup.demo.service;

import com.google.common.collect.ImmutableMap;
import hu.wup.demo.domain.Currency;
import hu.wup.demo.domain.CurrencyRate;
import hu.wup.demo.repository.CurrencyRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import static hu.wup.demo.domain.Currency.*;
import static hu.wup.demo.domain.Direction.BUY;
import static hu.wup.demo.domain.Direction.SELL;
import static java.math.BigDecimal.*;
import static java.util.stream.Collectors.toSet;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class CurrencyServiceTest {

    private static final BigDecimal SELL_EXCHANGE_RATE = new BigDecimal("222.33");
    private static final BigDecimal BUY_EXCHANGE_RATE = new BigDecimal("1111.4");
    @Mock
    private CurrencyRepository currencyRepository;

    @InjectMocks
    private CurrencyService underTest;

    @Test
    public void listCurrencies() {
        //GIVEN
        when(currencyRepository.findBySourceCurrency(USD)).thenReturn(createCurrencies(USD));

        //WHEN
        Map<Currency, BigDecimal> result = underTest.listCurrencies(USD, BUY);

        //THEN
        assertThat(result)
                .hasSize(4)
                .containsAllEntriesOf(
                        ImmutableMap.of(
                                JPY, BUY_EXCHANGE_RATE,
                                HUF, BUY_EXCHANGE_RATE,
                                EUR, BUY_EXCHANGE_RATE,
                                CAD, BUY_EXCHANGE_RATE));
    }

    @Test
    public void convertCurrencyReturnZeroWhenAmountIsZero() {
        //WHEN
        BigDecimal result = underTest.convertCurrency(USD, HUF, SELL, ZERO);

        //THEN
        assertThat(result).isEqualByComparingTo(ZERO);
    }

    @Test
    public void convertCurrencyReturnAmountWhenSourceAndTargetCurrencyAreEqual() {
        //WHEN
        BigDecimal result = underTest.convertCurrency(USD, USD, SELL, ONE);

        //THEN
        assertThat(result).isEqualByComparingTo(ONE);
    }

    @Test
    public void convertCurrencyCallsRepositoryOneTimeWhenSourceCurrencyIsHuf() {
        //GIVEN
        CurrencyRate currency = createCurrency(HUF, USD);
        when(currencyRepository.findBySourceCurrencyAndTargetCurrency(HUF, USD)).thenReturn(currency);

        //WHEN
        BigDecimal result = underTest.convertCurrency(HUF, USD, SELL, TEN);

        //THEN
        verify(currencyRepository, times(1)).findBySourceCurrencyAndTargetCurrency(HUF, USD);
        assertThat(result).isEqualByComparingTo(currency.getSellExchangeRate().multiply(TEN));
    }

    @Test
    public void convertCurrencyCallsRepositoryOneTimeWhenTargetCurrencyIsHuf() {
        //GIVEN
        CurrencyRate currency = createCurrency(USD, HUF);
        when(currencyRepository.findBySourceCurrencyAndTargetCurrency(USD, HUF)).thenReturn(currency);

        //WHEN
        BigDecimal result = underTest.convertCurrency(USD, HUF, SELL, TEN);

        //THEN
        verify(currencyRepository, times(1)).findBySourceCurrencyAndTargetCurrency(USD, HUF);
        assertThat(result).isEqualByComparingTo(currency.getSellExchangeRate().multiply(TEN));
    }

    @Test
    public void convertCurrency() {
        //GIVEN
        CurrencyRate usdToHufCurrency = createCurrency(USD, HUF);
        when(currencyRepository.findBySourceCurrencyAndTargetCurrency(USD, HUF)).thenReturn(usdToHufCurrency);

        CurrencyRate hufToEurCurrency = createCurrency(HUF, EUR);
        when(currencyRepository.findBySourceCurrencyAndTargetCurrency(HUF, EUR)).thenReturn(hufToEurCurrency);

        //WHEN
        BigDecimal result = underTest.convertCurrency(USD, EUR, SELL, TEN);

        //THEN
        verify(currencyRepository, times(2)).findBySourceCurrencyAndTargetCurrency(any(Currency.class), any(Currency.class));
        assertThat(result).isEqualByComparingTo(usdToHufCurrency.getSellExchangeRate().multiply(hufToEurCurrency.getSellExchangeRate()).multiply(TEN));
    }


    private Set<CurrencyRate> createCurrencies(Currency sourceCurrency) {
        return Arrays.stream(Currency.values())
                .map(currency -> createCurrency(sourceCurrency, currency))
                .filter(currencyRate -> !currencyRate.getTargetCurrency().equals(currencyRate.getSourceCurrency()))
                .collect(toSet());
    }

    private CurrencyRate createCurrency(Currency sourceCurrency, Currency targetCurrency) {
        return CurrencyRate.builder()
                .sourceCurrency(sourceCurrency)
                .targetCurrency(targetCurrency)
                .sellExchangeRate(SELL_EXCHANGE_RATE)
                .buyExchangeRate(BUY_EXCHANGE_RATE)
                .build();
    }

}
