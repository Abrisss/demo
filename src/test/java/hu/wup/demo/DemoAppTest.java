package hu.wup.demo;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import hu.wup.demo.domain.Currency;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;

import static hu.wup.demo.domain.Currency.*;
import static hu.wup.demo.domain.Direction.SELL;
import static io.restassured.RestAssured.given;
import static org.apache.http.HttpStatus.SC_OK;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = RANDOM_PORT)
public class DemoAppTest {

    private static final String HTTP_LOCALHOST = "http://localhost:";

    @Autowired
    private BaseDataGenerator baseDataGenerator;

    @LocalServerPort
    private int port;

    @Before
    public void setUp() throws Exception {
        baseDataGenerator.generate();
    }

    @Test
    public void listCurrencies() throws IOException {
        ImmutableMap<Currency, BigDecimal> expectedCurrencyMap = ImmutableMap.of(
                JPY, new BigDecimal("0.00769"),
                USD, new BigDecimal("0.00400"),
                EUR, new BigDecimal("0.00323"),
                CAD, new BigDecimal("0.00500"));

        String responseBodyJson =
                given()
                        .baseUri(HTTP_LOCALHOST + port)
                        .when()
                        .param("from", HUF)
                        .param("direction", SELL)
                        .get("/currencies")
                        .then()
                        .log().all()
                        .and()
                        .statusCode(200)
                        .and()
                        .extract().body().asString();


        Map<Currency, BigDecimal> resultCurrencyMap = new ObjectMapper().readValue(responseBodyJson, new TypeReference<Map<Currency, BigDecimal>>() {
        });

        assertThat(resultCurrencyMap)
                .hasSize(expectedCurrencyMap.size())
                .containsAllEntriesOf(expectedCurrencyMap);

    }

    @Test
    public void convertBackAndForthCurrency() {
        BigDecimal amount = new BigDecimal("111111111.1111");

        BigDecimal cadToHufResult =
                given()
                        .baseUri(HTTP_LOCALHOST + port)
                        .param("from", CAD)
                        .param("to", HUF)
                        .param("direction", SELL)
                        .param("amount", amount)
                        .when()
                        .get("/currencies/convert")
                        .then()
                        .log().all()
                        .and()
                        .statusCode(SC_OK)
                        .and()
                        .extract().body().as(BigDecimal.class);

        BigDecimal result =
                given()
                        .baseUri(HTTP_LOCALHOST + port)
                        .param("from", HUF)
                        .param("to", CAD)
                        .param("direction", SELL)
                        .param("amount", cadToHufResult)
                        .when()
                        .get("/currencies/convert")
                        .then()
                        .log().all()
                        .and()
                        .statusCode(SC_OK)
                        .and()
                        .extract().body().as(BigDecimal.class);

        assertThat(result).isEqualByComparingTo(amount);
    }
}